package com.example.estelle.cs_akazoo_app.playlists;

import java.util.ArrayList;

public interface PlaylistsPresenter {

    void getPlaylists();
}
