package com.example.estelle.cs_akazoo_app.playlists;

import java.util.ArrayList;

public class PlaylistInteractorImpl implements PlaylistInteractor{


    @Override
    public void getPlaylists(OnPlaylistsFinishListener listener) {
        ArrayList playlists = getMockedPlaylists();
        listener.onSuccess(playlists);

    }



    private ArrayList<Playlist> getMockedPlaylists() {
        ArrayList<Playlist> playlists = new ArrayList<Playlist>();
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        playlists.add(new Playlist("1", "Nisiotika", 11));
        playlists.add(new Playlist("2", "Rock", 14));
        playlists.add(new Playlist("3", "Pop", 9));
        playlists.add(new Playlist("4", "Metal", 8));
        return playlists;
    }
}
