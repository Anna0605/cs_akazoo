package com.example.estelle.cs_akazoo_app.tracks;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TracksFragment extends Fragment implements TracksView {

    RecyclerView mTracksRv;
    TracksPresenter presenter;



    public TracksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tracks, container, false);

        mTracksRv = v.findViewById(R.id.tracks_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTracksRv.setLayoutManager(layoutManager);




        return v;
    }


    @Override
    public void showTracks(ArrayList<Track> tracks) {
        TracksRvAdapter tracksRvAdapter = new TracksRvAdapter(tracks, new OnTracksClickListener() {
            @Override
            public void onTrackClicked(Track track) {
                Toast.makeText(getActivity(), "the track " + track.getTrackName() ,Toast.LENGTH_LONG).show();

            }

            @Override
            public void onTrackLogoClick(Track track) {
                Toast.makeText(getActivity(), "the track " + track.getTrackName() +" got clicked",Toast.LENGTH_LONG).show();
            }
        });
        mTracksRv.setAdapter(tracksRvAdapter);

    }


}
