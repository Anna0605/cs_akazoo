package com.example.estelle.cs_akazoo_app.playlists;

import java.util.ArrayList;

public interface PlaylistsView {

     void showPlaylists(ArrayList<Playlist> playlists);

}
