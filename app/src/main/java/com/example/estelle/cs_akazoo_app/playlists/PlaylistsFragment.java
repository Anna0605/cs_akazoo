package com.example.estelle.cs_akazoo_app.playlists;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsFragment extends Fragment implements PlaylistsView {

    RecyclerView playlistsRv;
    PlaylistsPresenter presenter;

    public PlaylistsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_playlists, container, false);

        playlistsRv = v.findViewById(R.id.playslists_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playlistsRv.setLayoutManager(layoutManager);

        presenter = new PlaylistsPresenterImpl(this);
       presenter.getPlaylists();

        return v;
    }




    @Override
    public void showPlaylists(ArrayList<Playlist> playlists) {

        PlaylistsRvAdapter playlistsRvAdapter = new PlaylistsRvAdapter(playlists, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClicked(Playlist playlist) {
                Toast.makeText(getActivity(), "the playlist" + playlist.getName() +" got clicked",Toast.LENGTH_LONG).show();

            }
        });
        playlistsRv.setAdapter(playlistsRvAdapter);

    }
}
