package com.example.estelle.cs_akazoo_app.tracks;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.playlists.PlaylistsRvAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class TracksRvAdapter extends RecyclerView.Adapter<TracksRvAdapter.TracksViewHolder> {

    private ArrayList<Track> tracks;
    private  OnTracksClickListener listener;

    public TracksRvAdapter(ArrayList<Track> tracks, OnTracksClickListener listener) {
        this.tracks = tracks;
        this.listener = listener;
    }

    public static class TracksViewHolder extends RecyclerView.ViewHolder {

        TextView mTracksName;
        TextView mTracksArtist;
        TextView mTracksCategory;
        ImageView mTracksLogo;
        RelativeLayout mTrackItemRoot;

        public TracksViewHolder(View v) {
            super(v);
            mTracksName = v.findViewById(R.id.track_name);
            mTracksArtist = v.findViewById(R.id.track_artist);
            mTracksCategory = v.findViewById(R.id.track_category);
            mTracksLogo = v.findViewById(R.id.track_logo);
            mTrackItemRoot =v.findViewById(R.id.track_item_root);

        }
    }

    @NonNull
    @Override
    public TracksViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_track_item, viewGroup, false);
        TracksRvAdapter.TracksViewHolder vh = new TracksRvAdapter.TracksViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TracksViewHolder viewHolder, int i) {
        final int position = i;
        viewHolder.mTracksName.setText(tracks.get(i).getTrackName());
        viewHolder.mTracksArtist.setText(tracks.get(i).getTrackArtist());
        viewHolder.mTracksCategory.setText(tracks.get(i).getTrackCategory());
        viewHolder.mTracksLogo.setImageResource(R.mipmap.ic_launcher);

        viewHolder.mTrackItemRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onTrackClicked(tracks.get(position));
                    }
                });
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }
}
