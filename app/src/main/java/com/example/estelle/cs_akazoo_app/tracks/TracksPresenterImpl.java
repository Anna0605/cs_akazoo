package com.example.estelle.cs_akazoo_app.tracks;

import java.util.ArrayList;

public class TracksPresenterImpl implements TracksPresenter {
    TracksView tracksView;


    public TracksPresenterImpl(TracksView tracksView) {
        this.tracksView = tracksView;
    }

    @Override
    public void getTracks() {
        tracksView.showTracks(addMockedTracks());
    }


    private ArrayList<Track> addMockedTracks() {
        ArrayList<Track>  tracks = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            Track track = new Track("Track Name " + i, "Track Artist " + i, "TheCategory " + i);
            tracks.add(track);
        }
        return tracks;
    }


    }