package com.example.estelle.cs_akazoo_app.playlists;

import java.util.ArrayList;

public class PlaylistsPresenterImpl implements PlaylistsPresenter , PlaylistInteractor.OnPlaylistsFinishListener {

    PlaylistsView playlistsView;
    PlaylistInteractor interactor;//to prosthesa

    public PlaylistsPresenterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;
        interactor = new PlaylistInteractorImpl();
    }



         @Override
         public void getPlaylists() {
          this.playlistsView=playlistsView;
            interactor.getPlaylists(this);
            }


    @Override
    public void onSuccess(ArrayList<Playlist> playlists) {
        playlistsView.showPlaylists(playlists);

    }

    @Override
    public void onError() {

    }
}
