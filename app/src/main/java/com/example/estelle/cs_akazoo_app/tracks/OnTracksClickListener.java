package com.example.estelle.cs_akazoo_app.tracks;

import com.example.estelle.cs_akazoo_app.playlists.Playlist;

public interface OnTracksClickListener {

    void onTrackClicked(Track track);

    void onTrackLogoClick(Track track);
}
