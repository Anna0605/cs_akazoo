package com.example.estelle.cs_akazoo_app.playlists;

public interface OnPlaylistClickListener {

    void onPlaylistClicked(Playlist playlist);
}
